/*
* GccApplication4.cpp
*
* Created: 03.05.2017 19:56:14
* Author : Юра
*/

#define F_CPU 16000000L
#define XTAL F_CPU
#define baudrate 9600L
#define bauddivider (XTAL/(16*baudrate)-1)
#define HI(x) ((x)>>8)
#define LO(x) ((x)& 0xFF)

#define CHR_ESC				0x1B
#define CHR_BS				0x08
#define CHR_CR				0x0D
#define CHR_LF				0x0A


#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <avr/interrupt.h>

#include <stdbool.h>
#include <avr/cpufunc.h>
#include <string.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/pgmspace.h>
#include "fifo.h"



//Global variables here

#define RX_BUFFER_SIZE 16
char rx_buffer[RX_BUFFER_SIZE];
unsigned char rx_wr_index,rx_rd_index,rx_counter;
char rx_buffer_overflow;
char read_enable = 0;
char process_data = 0;

volatile uint16_t second=0;

volatile uint16_t cutoff=2750;
uint16_t EEMEM cutoff_mem=2750;

volatile uint16_t changeV=3000;
uint16_t EEMEM changeV_mem=3000;

volatile uint16_t divider[4]={10000,11364,40714,10000};
uint16_t EEMEM r_mem = 10000;		//0=5v+-0.0048 1=5.6V+-0.0055 2=21v+-0.021
uint16_t EEMEM r1_mem= 11364;
uint16_t EEMEM r2_mem= 40714;

volatile unsigned char mode = 2;	//0=off 1=8ohm 2=4ohm 3=4/8ohm
unsigned char EEMEM mode_mem = 2;

volatile unsigned char dodischarge = 0; //0=off 1=on

volatile long double capacity=0;
volatile unsigned char resistance=0;

//res_int
volatile unsigned char calcres=0;
volatile double calcresdouble=0.0;
volatile unsigned int values_calcres[3]={0,0,0};
//res_int



FIFO( 256 ) uart_tx_fifo;
//FIFO( 32 ) uart_rx_fifo;

uint16_t uint16ifmem(uint16_t a,uint16_t max,uint16_t thenn ){if (a==max) {return thenn;}else{return a;}}
char chrifmem(char a,char max,char thenn ){if (a==max) {return thenn;}else{return a;}}
unsigned char uchrifmem(unsigned char a,unsigned char max,unsigned char thenn ){if (a==max) {return thenn;}else{return a;}}

void TimerInit(void)
{

	// Timer/Counter 1 initialization
	// Clock source: System Clock
	// Clock value: 8000,000 kHz
	// Mode: CTC top=OCR1A
	// OC1A output: Discon.
	// OC1B output: Discon.
	// Noise Canceler: Off
	// Input Capture on Falling Edge
	// Timer 1 Overflow Interrupt: On
	// Input Capture Interrupt: Off
	// Compare A Match Interrupt: Off
	// Compare B Match Interrupt: Off
	

	TIMSK=(1<<2); // enabled global and timer overflow interrupt;
	TCCR1A = 0x00; // normal operation page 148 (mode0);
	TCNT1=0x0BDC; // set initial value to remove time error (16bit counter register)
	TCCR1B = 0x04; // start timer/ set clock
}

void USARTInit(void)
{
	UBRRH = HI(bauddivider);
	UBRRL = LO(bauddivider);
	UCSRB = 1<<RXEN|1<<TXEN|1<<RXCIE;//|1<<TXCIE;
	//ucsrb recieveENABPWM transmitENABLE
	//recieveINTERenable transmitINTERdisable
	UCSRC = 1<<URSEL|1<<UCSZ0|1<<UCSZ1; //8bit
}

void uart_send( char data)
{
	FIFO_PUSH(uart_tx_fifo, data);
}
void uart_send_( char data)
{
	while(!( UCSRA & (1 << UDRE))); 
	UDR = data; 
}
void uart_send_str( char *s)
{
	while (*s != 0) uart_send(*s++);
}
void uart_send_str_(void)
{
	while (FIFO_COUNT(uart_tx_fifo) != 0)
	{
		uart_send_(FIFO_FRONT(uart_tx_fifo));
		FIFO_POP(uart_tx_fifo);
	}
}
void uart_clear_screen(void)
{
	uart_send(CHR_ESC);
	uart_send_str("[2J");
	uart_send(CHR_ESC);
	uart_send_str("[H");
}
void uart_clear_line(void)
{
	uart_send(CHR_ESC);
	uart_send_str("[2K");
	uart_send(CHR_ESC);
	uart_send_str("[99D");

}
void uart_new_line(void)
{
	uart_send(CHR_CR);
	uart_send(CHR_LF);
}

uint16_t strtoint(const char * str,unsigned char start,unsigned char count)
{
	char valuePWM[count];
	memcpy(valuePWM,&str[start],count);
	return atoi(valuePWM);
}

unsigned int getADC(void) 
{
	unsigned int v;
	
	ADCSRA|=(1<<ADSC);	
	
	while ((ADCSRA&(1<<ADIF))==0x00) 
	;
	
	v=(ADCL|ADCH<<8);
	return v;
}

unsigned int ADC_result(void)
{
	ADCSRA|=0x40;
	while((ADCSRA & 0x10)==0);
	ADCSRA|=0x10;
	return ADCW;
}


ISR(TIMER1_OVF_vect)
{
	cli();
	TCNT1=0x0BDC;
	
	if (calcres)
	{
		unsigned int adcres=ADC_result();
		long double voltage=adcres*5.000/10230.000*divider[3];
		
		char str1[30];
		sprintf(str1, "%d; %.1fmV; %.1fOhm",second,voltage,resistance/10.0);
		uart_send_str(str1);
		uart_new_line();
		
		if (second==0)
		{
			values_calcres[0]=adcres;
		}
		if (second<7)
		{
			PORTC |= (1 << PORTC0);
			PORTC &= ~(1 << PORTC1);
			PORTC &= ~(2 << PORTC2);
			resistance=82;
		}
		else if (second==7)
		{
			values_calcres[1]=adcres;
		}
		else if (second<14)
		{
			PORTC |=(1<<PORTC0);
			PORTC |=(1<<PORTC1);
			PORTC &= ~(1 << PORTC2);
			resistance=41;
		}
		else if (second==14)
		{
			values_calcres[2]=adcres;
			PORTC &= ~(1 << PORTC0);
			PORTC &= ~(1 << PORTC1);
			PORTC &= ~(1 << PORTC2);
			resistance=0;
			double v1=values_calcres[1]*5.000/10230.000*divider[3];
			double v2=values_calcres[2]*5.000/10230.000*divider[3];
			if ((v2>0) &&(v1>0))
			calcresdouble=(v1-v2)/((v2/4.1F)-(v1/8.2F))*1000.00;
			else calcresdouble=0.0;
			char str0[15];
			sprintf(str0, "%.1f mOhm",calcresdouble);
			uart_send_str(str0);
			uart_new_line();
			calcres=0;
		}
		
		

		
		
		second++;
	}
	else
	{
		unsigned int adcres=ADC_result();
		long double voltage=adcres*5.000/10230.000*divider[3];
		unsigned int current=0;
		
		if (dodischarge)
		{
			
			if (mode==0) //off
			{
				PORTC &= ~(1 << PORTC0);
				PORTC &= ~(1 << PORTC1);
				PORTC &= ~(1 << PORTC2);
				dodischarge=0;
				resistance=0;
			}
			if (mode==1) //8ohm
			{
				PORTC |= (1 << PORTC0);
				PORTC &= ~(1 << PORTC1);
				PORTC &= ~(2 << PORTC2);
				resistance=82;
			}
			if (mode==2) //4ohm
			{
				PORTC |=(1<<PORTC0);
				PORTC |=(1<<PORTC1);
				PORTC &= ~(1 << PORTC2);
				resistance=41;
			}
			if (mode==3) //4-8
			{
				PORTC |=(1<<PORTC0);
				PORTC |=(1<<PORTC1);
				PORTC &= ~(1 << PORTC2);
				resistance=41;
				if (voltage<(changeV)) mode=1;
			}
			if (mode==4) //22ohm
			{
				PORTC &= ~(1 << PORTC0);
				PORTC &= ~(1 << PORTC1);
				PORTC |=(1<<PORTC2);
				resistance=220;
			}
			if ((adcres>0)&&(resistance>0))
			{
				capacity+=voltage/(resistance/10.000000000000)/60.000000000000/60.000000000000;
				current=voltage/(resistance/10.000000000000);
			}
			

			
			
			if (voltage<(cutoff)) mode=0;
			
			char str1[30];
			sprintf(str1, "%d; %.1fmV; %.1fOhm; %.2fA; %.1fmAh",second,voltage,resistance/10.0,current/1000.0000,capacity);
			uart_send_str(str1);
			uart_new_line();
			second++;
		}
	}
	sei();
}

ISR(USART_RXC_vect)
{
	cli();
	char data=UDR;
	
	if ((data == 'x') || (data == 'X'))
	{
		memset(&rx_buffer[0], 0, sizeof(rx_buffer));
		rx_wr_index = 0;
		read_enable = 0;
		uart_clear_line();
	}
	else{
		if (read_enable == 0)
		{
			memset(&rx_buffer[0], 0, sizeof(rx_buffer));
			rx_wr_index = 0;
			read_enable = 1;
		}
		if(((data == 13)||((data == 10)))&&(read_enable == 1))
		{
			uart_send('.');
			uart_new_line();
			uart_send('>');
			read_enable = 0;
			process_data = 1;
		}
		
		if (read_enable == 1)
		{
			uart_send(data);
			rx_buffer[rx_wr_index++]=data;
			if (rx_wr_index == RX_BUFFER_SIZE)
			rx_wr_index=0;
			if (++rx_counter == RX_BUFFER_SIZE)
			{
				rx_counter=0;
				rx_buffer_overflow=1;
			}
		}
	}
	sei();
}

int main(void)
{
	
	//ddr 1=output 0=input
	//port if output, 1=1; 0=0, if input 0=Hi-Z; 1=pullup 100k +V
	//PORTD |= (1 << PD7);		1
	//PORTD &= ~(1 << PD7);		0
	
	cli();

	DDRB = (0<<PORTB0)|(0<<PORTB1)|(0<<PORTB2)|(0<<PORTB3)|(0<<PORTB4)|(0<<PORTB5)|(0<<PORTB6)|(0<<PORTB7);
	DDRC = (1<<PORTC0)|(1<<PORTC1)|(1<<PORTC2)|(0<<PORTC3)|(0<<PORTC4)|(0<<PORTC5);//|(0<<PORTC6)|(0<<PORTC7); C6=Reset, C7 not exist
	DDRD = /*(0<<PORTD0)|(0<<PORTD1)|*/(0<<PORTD2)|(0<<PORTD3)|(0<<PORTD4)|(0<<PORTD5)|(0<<PORTD6)|(0<<PORTD7); //D0,D1 USART
	
	PORTB= (1<<PORTB0)|(1<<PORTB1)|(1<<PORTB2)|(1<<PORTB3)|(1<<PORTB4)|(1<<PORTB5)|(1<<PORTB6)|(1<<PORTB7);
	PORTC= (0<<PORTC0)|(0<<PORTC1)|(0<<PORTC2)|(1<<PORTC3)|(1<<PORTC4)|(1<<PORTC5);//|(1<<PORTC6)|(1<<PORTC7);
	PORTD= /*(1<<PORTD0)|(1<<PORTD1)|*/(1<<PORTD2)|(1<<PORTD3)|(1<<PORTD4)|(1<<PORTD5)|(1<<PORTD6)|(1<<PORTD7);
	

	TimerInit();
	USARTInit();
	ADCSRA=(1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
	//Включаем АЦП, тактовая частота бреобразователя =/128 от тактовой микроконтроллера
	ADMUX=(0<<REFS1)|(1<<REFS0)|(0<<MUX0)|(1<<MUX1)|(1<<MUX2)|(0<<MUX3);
	
	
	
	cutoff = uint16ifmem(eeprom_read_word(&cutoff_mem),0xffff,2750);
	changeV = uint16ifmem(eeprom_read_word(&changeV_mem),0xffff,3000);
	divider[1] = uint16ifmem(eeprom_read_word(&r1_mem),0xffff,11364);
	divider[2] = uint16ifmem(eeprom_read_word(&r2_mem),0xffff,40714);

	divider[3] = uint16ifmem(eeprom_read_word(&r_mem),0xffff,10000);
	mode = chrifmem(eeprom_read_byte(&mode_mem),0xff,3);

	sei();

	while(1)
	{
		if (FIFO_COUNT(uart_tx_fifo)!=0) uart_send_str_();
		if (process_data==1)
		{
			process_data=0;
			if (!strncmp(rx_buffer,"ma",2))	//ma 1	//ma 0=resistance 1=discharge
			{										//012345
				calcres=strtoint(rx_buffer,3,1);
			}
			else if (!strncmp(rx_buffer,"mo",2))	//mo 4	//mode 0=off 1=8ohm 2=4ohm 3=4/8ohm 4=22ohm
			{										//012345
				mode=strtoint(rx_buffer,3,1);
				eeprom_write_byte(&mode_mem, mode);
			}
			else if (!strncmp(rx_buffer,"cu",2))	//cu 2700 //cutoff voltage battery
			{										//0123456
				cutoff=strtoint(rx_buffer,3,5);
				eeprom_write_word(&cutoff_mem, cutoff);
			}
			else if (!strncmp(rx_buffer,"ch",2))	//ch 3000 //change voltage in mode 3 from 4ohm to 8ohm
			{										//0123456
				changeV=strtoint(rx_buffer,3,5);
				eeprom_write_word(&changeV_mem, changeV);
			}
			else if (!strncmp(rx_buffer,"r1",2))	//r1 1136 //change voltage in mode 3 from 4ohm to 8ohm
			{										//01234567
				divider[1]=strtoint(rx_buffer,3,5);
				eeprom_write_word(&r1_mem, divider[1]);
			}
			else if (!strncmp(rx_buffer,"r2",2))	//r2 4013 //change voltage in mode 3 from 4ohm to 8ohm
			{										//01234567
				divider[2]=strtoint(rx_buffer,3,5);
				eeprom_write_word(&r2_mem, divider[2]);
			}
			else if (!strncmp(rx_buffer,"di",2))	//di 1 //change divider type to 0=5v+-0.0048 1=5.6V+-0.0055 2=21v+-0.021
			{										//0123
				divider[3]=divider[strtoint(rx_buffer,3,1)];
				eeprom_write_word(&r_mem, divider[3]);
			}
			
			//5*(7300/(7300+1000))		//k=5/(5*(7300/(7300+1000)))	==1.13698630137	//11369 //4.205/3.70 //11364
			//20*(7300/(7300+22000))	//k=20/(20*(7300/(7300+22000)))	==4.01369863014	//40136 //14.25/3.50 //40714
			else if (!strncmp(rx_buffer,"clear",5)) {capacity=0;second=0;}
			else if (!strncmp(rx_buffer,"cl",2)) uart_clear_screen();

			else if (!strncmp(rx_buffer,"start",5)) dodischarge=1;
			else if (!strncmp(rx_buffer,"stop",4)) dodischarge=0;
			else if (!strncmp(rx_buffer,"in",2))
			{
				char str1[100];
				sprintf(str1, "mo:%d div:%.2f cut:%dm 4to8:%u cap:%.1f res:%.1f",mode,divider[3]/10000.000*5.00,cutoff,changeV,capacity,calcresdouble);
				uart_send_str(str1);
				uart_new_line();
			}
			// 			else if (!strncmp(rx_buffer,"ee",2))
			// 			{
			// 				char str1[10];
			// 				sprintf(str1, "%d",strtoint("uu 1234",3,3));
			// 				uart_send_str(str1);
			// 				uart_new_line();
			// 			}


			else
			{
				uart_send_str("Unkn cmd");
				uart_new_line();
				
			}
		}
	}
}
